# BEATLES CITATIONS

## Installation
Clone the plugin in the zsh custom plugin folder
```bash
cd ~/.oh-my-zsh/custom/plugins/
git clone git@gitlab.com:GuillaumeRdg/zsh-beatles.git beatles
```
Add it into your `.zshrc` file inside the plugins section
```
plugins(
    ... 
    beatles
)
```

This plugin require fortune, to install it run 
```bash
brew install fortune
```

## Use
To see a beatles citation simply run 
```bash
beatles
```
