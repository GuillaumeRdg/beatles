# beatles: Beatles citations fortunes

BEATLES_PLUGIN_DIR=${0:h}

() {
local DIR=$BEATLES_PLUGIN_DIR/fortunes
if [[ ! -f $DIR/beatles.dat ]] || [[ $DIR/beatles.dat -ot $DIR/beatles ]]; then
  local strfile=strfile
  if ! which strfile &>/dev/null && [[ -f /usr/sbin/strfile ]]; then
    strfile=/usr/sbin/strfile
  fi
  if which $strfile &> /dev/null; then
    $strfile $DIR/beatles $DIR/beatles.dat >/dev/null
  else
    echo "[oh-my-zsh] beatles depends on strfile, which is not installed" >&2
    echo "[oh-my-zsh] strfile is often provided as part of the 'fortune' package" >&2
  fi
fi

alias beatles="fortune -a $DIR"
}

unset BEATLES_PLUGIN_DIR
